# arc() vẽ cung tròn vaofdduwongf dẫn phụ hiện tại

- arc(x, y, radius, startAngle, endAngle)
- arc(x, y, radius, startAngle, endAngle, counterclockwise)
x: Tọa độ ngang của tâm cung
y:  Tọa độ dọc của tâm cung
radius: bán kính của cung
startAngle: Góc tại đó cung bắt đầu tính bằng radian, được đo từ trục x dương
endAngle: Góc tại đó cung kết thúc tính bằng radian, được đo từ trục x dương
couterclockwise : Một giá trị boolean tùy chọn. Nếu đúng, vẽ cung ngược chiều kim đồng hồ giữa góc đầu và góc cuối. Mặc định là sai (theo chiều kim đồng hồ)
# beginPath() tạo đường bắt đầu
# closePath() nối điểm cuối với điểm đầu

#  bezierCurveTo() hàm vẽ đường cong

ctx.bezierCurveTo(cp1.x, cp1.y, cp2.x, cp2.y, end.x, end.y)

# clearRect() xóa các pixel trong vùng hình chữ nhật bằng cách đặt chúng thành màu đen trong suốt

clearRect(x, y, width, height)

# clip() bó các khối vẽ trong phạm vi

clip()
clip(path)
clip(fillRule)
clip(path, fillRule)