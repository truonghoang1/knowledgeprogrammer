# canvas :HTMLcanvasElement

const canvas = document.getElementById("canvas");
// ctx hiểu đơn giản  coi nó là cây bút để vẽ lên mặt canvas 2d
const ctx = canvas.getContext("2d");

# ctx direction viết text theo trái qua phải và từ phải qua trái
 ctx.direction = 'rtl' || 'ltr' ;

# ctx.fillStyle : đổ màu cho một khối

ctx.fillStyle = 'blue' ;
// multiple colors

for (let i = 0; i < 6; i++) {
  for (let j = 0; j < 6; j++) {
    ctx.fillStyle = `rgb(
        ${Math.floor(255 - 42.5 * i)}
        ${Math.floor(255 - 42.5 * j)}
        0)`;
    ctx.fillRect(j * 25, i * 25, 25, 25);
  }
}

# ctx.font =" " font chữ cho text
ctx.font = "bold 48px serif";
ctx.strokeText("Hello world", 50, 100);