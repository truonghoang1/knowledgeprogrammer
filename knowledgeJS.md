# muốn dừng vòng lặp forEarch
- dùng try-catch throw new Error
- thay thế bằng for..of :( tương thích vs mảng strings, map ,sets, and so on, và dùng break, continuem return)
- thay bằng some(), every()

# biến được lưu như nào
- Biến nguyên thủy như số, chuỗi, boolean thường được lưu trữ trực tiếp trên stack, nơi quản lý các biến theo cấp độ ngăn xếp (stack frame).
Đối tượng và các biến được tạo động thường được lưu trữ trên heap, một vùng bộ nhớ lớn hơn, và trên stack, chỉ lưu trữ địa chỉ trỏ đến vùng nhớ trên heap

- Nếu bạn tạo một closure trong một hàm, các biến của hàm cha sẽ được lưu trữ trong bộ nhớ để có thể sử dụng bởi các hàm con. Cơ chế này được gọi là lexical scope
Mỗi khi một hàm được gọi, một execution context mới được tạo ra.
-  Execution context chứa thông tin về biến, hàm, this, và môi trường khác liên quan đến việc thực thi hàm đó. Mỗi execution context có thể chứa các biến cục bộ và tham số của hàm.
- Khi một đối tượng được tạo từ một class hoặc hàm constructor, nó có một prototype (một đối tượng khác) được liên kết với nó. Các thuộc tính không được tìm thấy trực tiếp trên đối tượng sẽ được tìm kiếm trong prototype chain. Mối quan hệ này giúp chia sẻ các phương thức giữa nhiều đối tượng mà không cần lưu trữ lại mỗi lần.

- biến khai báo pass by value và pass by reference
- exp:
 +  let a =10; let b= a; a=20 => a=20;b=10
 + đây gọi là khởi tạo thông qua tham trị tức là địa chỉ b sẽ lưu giá trị của a , vậy khi a thay đổi thì b không bị thay đổi theo a
 + let a = obj A ,b=a  => a.value =20 ,b.value=20
 + đây là khởi tạo qua tham chiếu tức là b lúc này chỉ lưu địa chỉ của a chứ không lưu giá trị của a vì vậy khi a thay đổi thì b cũng thay đổi theo
 
# sự khác nhau giữa promise.all và promise.allSettled
- promise.allSettled đợi cho các promise ổn định rồi trả ra kết quả thông qua check fulfilled và reason
- promise.all trả ra reject ngay khi phát hiện lỗi
# stack 
- Ngăn xếp là một vùng bộ nhớ nhỏ, có tổ chức. Đó là nơi lưu trữ các giá trị nguyên thủy, lệnh gọi hàm và biến cục bộ Nó tuân theo thứ tự "Vào cuối, ra trước" (LIFO), nghĩa là mục cuối cùng được thêm vào ngăn xếp là mục đầu tiên bị xóa
- Mỗi lệnh gọi hàm tạo ra một khung ngăn xếp mới, chứa các biến cục bộ của hàm, địa chỉ trả về và dữ liệu theo ngữ cảnh khác
# heap 
- Heap là một vùng bộ nhớ lớn, hầu hết không có cấu trúc. Đó là nơi lưu trữ các đối tượng, mảng và hàm, Các biến từ Ngăn xếp (ví dụ: trong hàm) trỏ đến các vị trí trong Heap cho các cấu trúc được phân bổ động này
- Khi bạn khai báo một kiểu nguyên thủy (như số hoặc boolean), kiểu đó thường được quản lý trong ngăn xếp. Nhưng khi bạn tạo một đối tượng, mảng hoặc hàm, nó sẽ được lưu trong heap và ngăn xếp sẽ chứa tham chiếu đến vị trí đó trong heap

# prototypes chain js
  
- Khi một thuộc tính hoặc phương thức được truy cập trên một đối tượng, trước tiên JavaScript sẽ kiểm tra chính đối tượng đó. Nếu không tìm thấy ở đó, nó sẽ tra cứu thuộc tính hoặc phương thức trong nguyên mẫu của đối tượng
- Quá trình này tiếp tục, di chuyển chuỗi từ nguyên mẫu này sang nguyên mẫu tiếp theo, cho đến khi tìm thấy thuộc tính hoặc phương thức hoặc đến cuối chuỗi (thường là nguyên mẫu của đối tượng cơ sở, là null)
- Chuỗi nguyên mẫu là nền tảng cho mô hình kế thừa nguyên mẫu của JavaScript, cho phép các đối tượng kế thừa các thuộc tính và phương thức từ các đối tượng khác

# closure 
- Bao đóng là một hàm có quyền truy cập vào phạm vi hàm bên ngoài của nó ngay cả sau khi hàm bên ngoài đã trả về. Điều này có nghĩa là một bao đóng có thể ghi nhớ và truy cập các biến và đối số của hàm ngoài của nó ngay cả khi hàm đã kết thúc.