# khái niệm (definition):
-  Internet là một mạng lưới toàn cầu gồm các máy tính được kết nối với nhau thông qua một bộ giao thức được tiêu chuẩn hóa
- ( The Internet is a global network of computers connected to each other which communicate through a standardized set of protocols )
# cách hoạt động (how does the internet work)
- internet kết nối thiết bị và hệ thống máy tính cùng nhau sử dụng tập các phương thức tiêu chuẩn hóa
- Cốt lõi của Internet là một mạng lưới toàn cầu gồm các bộ định tuyến được kết nối với nhau, chịu trách nhiệm điều hướng lưu lượng giữa các thiết bị và hệ thống khác nhau
- Để đảm bảo các gói được gửi và nhận chính xác, internet sử dụng nhiều giao thức khác nhau, bao gồm Giao thức Internet (IP) và Giao thức điều khiển truyền dẫn (TCP)
# basic concepts and terminology ( thuật ngữ cơ bản)
- Packet: A small unit of data that is transmitted over the internet.
- Router: A device that directs packets of data between different networks.
- IP Address: A unique identifier assigned to each device on a network, used to route data to the correct destination.
- Domain Name: A human-readable name that is used to identify a website, such as google.com.
- DNS: The Domain Name System is responsible for translating domain names into IP addresses.
- HTTP: The Hypertext Transfer Protocol is used to transfer data between a client (such as a web browser) and a server (such as a website).
- HTTPS: An encrypted version of HTTP that is used to provide secure communication between a client and server.
- SSL/TLS: The Secure Sockets Layer and Transport Layer Security protocols are used to provide secure communication over the internet.